"use strict";

// Put your DOMContentLoaded event listener here first.
function setup(e){
  let curPlayer = "x";
  const h2 = document.getElementsByTagName("h2")[1];
  h2.classList.add("hide");
  const turn = document.getElementById("curplayer");
  const tbody = document.getElementsByTagName("tbody")[0];
  const positions = [];

  

  function clicked(e){
    if(e.target.textContent !== ""){
      return;
    }
    else if(h2.className !== "hide"){
      return;
    }
    else if(e.target.textContent === ""){
      e.target.textContent = curPlayer;
      e.target.classList.add(`${curPlayer}square`);
      if(curPlayer === "x"){
        curPlayer = "o";
      }
      else{
        curPlayer = "x";
      }
      for(let i = 0; i < 9;i++){
        if(document.getElementsByTagName("td")[i].textContent === ""){
          positions[i] = false;
          console.log(positions[i]);
        }
        else{
          positions[i] = document.getElementsByTagName("td")[i].textContent;
          console.log(positions[i]);
        }
      }
      checkBoard(...positions);
      turn.textContent = curPlayer;
    }
  }

  tbody.addEventListener("click", clicked);
  
}
document.addEventListener("DOMContentLoaded", setup);